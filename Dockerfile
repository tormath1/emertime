FROM python:3.7.2-alpine3.9

RUN apk add --update postgresql-dev gcc musl-dev

COPY requirements.txt /tmp/requirements.txt
RUN  pip install --requirement /tmp/requirements.txt

WORKDIR /opt/
COPY . .

CMD ["python", "main.py"]